# doc-whgp2018-misc

This is an automated passenger car reservation system, which is similar to the Grab but more compact. System applicate a technology for IoT and very fast in relay message. Seem to be, developers was cause a problem in flow of application, which's lead to a fork for user. That's so bad. We have to find out what's that and "some information in logs" which's important was be leak. Following, something about the application was published in here:


# **Registration:**
[Link Register](http://45.76.149.132:7331)

    + API: /api/v1/register
```json
{
    "username": <your_username>,
    "passhash": <your_passhash>,
    "type": driver/user,
    "client_id": <your_client_id>
}
```
### Note: len("your_client_id") <= 12

# **Broker**
[Link Broker](mqtt://45.76.149.132:8383)

    + Topic access: whgp2018/<your_id>/user  - If you registered with type is "user" \n
    + Topic access: whgp2018/<your_id>/driver  - If you registered with type is "driver"

### Note: Don't worry because, you have only 1 access to specified "topic" until you are being grant access. haha

# **User Block Function:**
========================================
* User tracking:
```json
    {
      "check": "Client",
      "action": "tracking",
      "location": {
        "lon": <longtidude>,
        "lat": <latidude>
      }
    }
```

* User book a trip:
```json
    {
      "check": "Client",
      "action": "booking",
      "myLocation": {
        "lon": <longtidude>,
        "lat": <latidude>
      },
      "distInfor": {
        "from": {
          "lon": <longtidude>,
          "lat": <latidude>,
          "addrFrom": "Address from A"
        },
        "to": {
          "lon": <longtidude>,
          "lat": <latidude>,
          "addrTo": "Address to B"
        },
        "distance": 3,
        "distUnit": "km"
      }
    }
```

* User find drivers
```json
    {
      "check": "Client",
      "action": "findDrivers"
    }
```


# **Drivers Block Function:**
========================================
* Driver up location
```json
    {
      "check": "driver",
      "action": "upLocation",
      "location": {
        "lon": <longtidude>,
        "lat": <latidude>
      }
    }
```

* Driver accept trip
```json
{
    "check": "driverID",
    "action": "acceptTrip",
    "idTrip": "<tripID>"
}
```

### **Note:**
+ After registration, access to broker by username,password and client_id. You can choose "driver" or "user"
+ Call function by template which is described in above.